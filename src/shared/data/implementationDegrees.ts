interface Degreee {
  label: string;
  value: string;
}

export const implementationDegreesData: Degreee[] = [
  {
    value: 'T',
    label: 'Totalmente implementado',
  },
  {
    value: 'L',
    label: 'Largamente implementado',
  },
  {
    value: 'P',
    label: 'Parcialmente implementado',
  },
  {
    value: 'N',
    label: 'Não implementado',
  },
  {
    value: 'NA',
    label: 'Não avaliado',
  },
  {
    value: 'F',
    label: 'Fora do escopo implementado',
  },
];
