interface Capacity {
  label: string;
  value: string;
}

export const capacitiesData: Capacity[] = [
  {
    value: 'O',
    label: 'Organizacional',
  },
  {
    value: 'P',
    label: 'Projeto',
  },
];
