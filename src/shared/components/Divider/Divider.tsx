import styled from 'styled-components';

export const Divider = styled.hr`
  height: 2px;
  width: 100%;
  position: relative;
  top: -20px;

  border-style: none;
  border-radius: 20px;
  background-color: var(--gray-100);
`;
