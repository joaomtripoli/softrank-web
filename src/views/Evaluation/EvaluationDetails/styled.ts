import styled from 'styled-components';

export const ERTitle = styled.h3`
  font-size: 18px;
  color: var(--gray-700);
`;
